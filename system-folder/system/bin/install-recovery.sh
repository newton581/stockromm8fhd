#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:270a3770929c7613eb4855bff8caf81c33fcb5bb; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:29e23a9c1593daff7c2ffb615759f6ec77761cf3 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:270a3770929c7613eb4855bff8caf81c33fcb5bb && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
